# Elastic Stack Version 7.3 Docker 

This docker-compose runs the 7.3 version of the Elastic Stack (Elastic Search, Logstash, Kibana). As an example use case, we consider data that is coming in as a semicolon-separated string via MQTT. The first two numbers are GPS coordinates, the last three sensor values. Logstash will parse the data and save it into a csv file output/out.csv as well as the Elasticsearch-database. The data can then be visualized with Kibana. Adjustments in the input, output and data parsing should be made in ```logstash/pipeline/logstash.conf```.

Caution: Make sure that ELK can write in the directories that you are running the stack. The following commands can be helpful for debugging:
- ```docker container ls``` to get the IDs of the containers.
- ```docker logs <container ID>``` to inspect the logs.

- start by calling the shell-script ```./start```
- open Kibana in the browser: http://localhost:5601
- the containers can be shut down with ```./stop```

In order to map the location as geo_point (so that it can be displayed in maps), we need to set the mapping correctly. If the index 'data' (which is used according to our logstash-config) is not set yet (no data yet), run the following commands (after starting the stack):
- ```curl -X PUT "http://localhost:9200/data?pretty" -d @logstash/config/mapping.json -H 'Content-Type: application/json'```
- ```docker restart elk_logstash_1```

If it is already used, you need to reindex:
- ```curl -X PUT "http://localhost:9200/new_data?pretty" -d @logstash/config/mapping.json -H 'Content-Type: application/json'```
- ```curl -X POST "http://localhost:9200/_reindex?pretty" -d @logstash/config/reindex.json -H 'Content-Type: application/json'```
- You need to adjust ```logstash/pipeline/logstash.conf``` to store into the new index ```new_data``` instead of the old ```data```.
- You might then want to delete the old index ```data```.
